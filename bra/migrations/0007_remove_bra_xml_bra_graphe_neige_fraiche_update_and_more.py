# Generated by Django 4.2.7 on 2023-11-13 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bra', '0006_bra_xml_content_alter_bra_risk_img_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bra',
            name='xml',
        ),
        migrations.AddField(
            model_name='bra',
            name='graphe_neige_fraiche_update',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bra',
            name='montagne_enneigement_update',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bra',
            name='montagne_risques_update',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bra',
            name='rose_pentes_update',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bra',
            name='sept_derniers_jours_update',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bra',
            name='xml_update',
            field=models.IntegerField(default=0),
        ),
    ]
