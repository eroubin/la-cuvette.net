# Generated by Django 4.2.7 on 2023-11-13 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bra', '0005_alter_bra_risk_img_alter_bra_snow_img'),
    ]

    operations = [
        migrations.AddField(
            model_name='bra',
            name='xml_content',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='bra',
            name='risk_img',
            field=models.TextField(blank=True, default='risk_img', null=True),
        ),
        migrations.AlterField(
            model_name='bra',
            name='snow_img',
            field=models.TextField(blank=True, default='snow_img', null=True),
        ),
    ]
