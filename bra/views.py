from django.shortcuts import render
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt

import logging
import json

from webcuvette import settings
from skitour.models import Mountain
from bra.models import Bra

log = logging.getLogger(__name__)


@csrf_exempt
def bra(request):
    http_status = 200
    try:
        # get domain id from payload
        payload = json.loads(request.body)
        log.debug(f'request body: {payload}')
        mountain_id = int(payload["mountain_id"])

        # cache
        cache_key = f'bra-{mountain_id}'
        context = False if settings.DEBUG else cache.get(cache_key)
        if context:
            log.info(f'[cache] hit {cache_key}')
        else:
            mountain = Mountain.objects.filter(skitour_id=mountain_id).first()
            if mountain is None:
                raise Exception(f"Unknown mountain id {mountain_id}")

            log.debug(f'Mountain {mountain}')
            bras = mountain.bras.all()
            if not len(bras):
                raise Exception("No BRA found")

            context = {"bras": bras}
            cache.set(cache_key, context, settings.LOW_LEVEL_CACHE)
            log.info(f'[cache] set {cache_key}')

    except Exception as e:
        log.error(e)
        http_status = 500
        context = {"error": {
            "error": e,
            "msg": "Server error",
        }}

    return render(request, 'bra/bras.html', context, status=http_status)


# @csrf_exempt
def bras(request):
    http_status = 200
    try:
        context = {"bras": Bra.objects.all()}

    except Exception as e:
        log.error(e)
        http_status = 500
        context = {"error": {
            "error": e,
            "msg": "Server error",
        }}

    return render(request, 'bra/list.html', context, status=http_status)
