from django.apps import AppConfig


class BraConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bra'
