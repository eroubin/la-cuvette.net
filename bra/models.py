from django.db import models

import datetime
import pytz
import logging
import time
import xml.etree.ElementTree as ET
from webcuvette.api import meteofrance_api_call


class Bra(models.Model):
    meteofrance_id = models.IntegerField(default=0)

    name = models.CharField(max_length=64, default="name")
    date = models.IntegerField(default=0)
    validity = models.IntegerField(default=0)
    risk_max = models.IntegerField(default=0)

    # comments
    comment_acc = models.TextField(default="accidental")
    comment_nat = models.TextField(default="natural")

    # snow condition
    snow_quality = models.TextField(default="quality")

    # bra source
    xml_content = models.TextField(default='', blank=True, null=True)
    xml = models.FileField(upload_to='xml', blank=True, null=True)
    pdf = models.FileField(upload_to='pdf', blank=True, null=True)

    update = models.IntegerField(default=0)

    log = logging.getLogger(__name__ + ".Bra")

    def __str__(self):
        return f'BRA{self.padded_id()}: {self.name}'

    def padded_id(self):
        return f'{self.meteofrance_id:02d}'

    def get_pdf_name(self):
        return f'BRA{self.padded_id()}.pdf'

    def get_xml_name(self):
        return f'BRA{self.padded_id()}.xml'

    def get_pdf_url(self):
        return f'/media/bra/pdf/{self.get_pdf_name()}'

    def get_xml_url(self):
        return f'/media/bra/xml/{self.get_xml_name()}'

    def get_risk_max_icon(self):
        return f"/media/bra/icons/R{self.risk_max}.png"

    def is_up_to_date(self):
        hours_since_update = (time.time() - self.update) / 3600
        return hours_since_update < 24
    
    def update_from_api(self):
        try:
            hours_since_update = (time.time() - self.update) / 3600
            self.log.info(f'{self} Last update: {hours_since_update:.2f} hours ago')
            if self.is_up_to_date():
                self.log.debug(f'{self} skip update ({hours_since_update:.2f})')
                return True

            images = [
                "montagne-risques",
                "rose-pentes",
                "montagne-enneigement",
                "graphe-neige-fraiche",
                "sept-derniers-jours",
            ]
            urls_bra = [
                f"https://public-api.meteofrance.fr/public/DPBRA/v1/massif/BRA?id-massif={self.meteofrance_id}&format=xml",
                f"https://public-api.meteofrance.fr/public/DPBRA/v1/massif/BRA?id-massif={self.meteofrance_id}&format=pdf",
            ]
            urls_images = [
                f"https://public-api.meteofrance.fr/public/DPBRA/v1/massif/image/{image}?id-massif={self.meteofrance_id}" for image in images
            ]
            responses = meteofrance_api_call("get", urls_bra + urls_images)
            if not responses:
                self.log.error(f'{self} bra from api (responses is False)')
                return False

            for r in responses:
                r.raise_for_status()

            # xml
            r = responses[0]
            self.xml.name = f'bra/xml/{self.get_xml_name()}'
            with self.xml.open(mode='wb') as f:
                f.write(r.content)
            self.xml_content = r.text
            self.log.info(f'{self} xml updated')
            self.log.info(f'{self} save xml {self.xml.name}')

            # pdf
            r = responses[1]

            self.pdf.name = f'bra/pdf/{self.get_pdf_name()}'
            with self.pdf.open(mode='wb') as f:
                f.write(r.content)

            self.log.info(f'{self} save pdf {self.pdf.name}')

            for i, image in enumerate(images):
                r = responses[i + len(urls_bra)]
                r.raise_for_status()
                image_name = f'{image.replace("-", "_")}'
                with open(f"./webcuvette/media/bra/xml/{image_name}_{self.meteofrance_id}.png", "wb") as f:
                    f.write(r.content)

                self.log.info(f'{self} image {image} updated')

            self.update = self.date
            self.save()
            self.log.info(f'{self} fully updated')

        except Exception as e:
            self.log.error(f'{self} failed to get pdf/xml ({e})')
            return False

        return True

    def update_from_xml(self):
        try:
            tree = ET.fromstring(self.xml_content)
        except Exception as e:
            self.log.error(f'{self} can\'t update from xml ({e})')
            return

        def get_timestamp(bra_date):
            tz = pytz.timezone("Europe/Paris")
            d = datetime.datetime.strptime(bra_date, "%Y-%m-%dT%H:%M:%S").replace(tzinfo=tz)
            return datetime.datetime.timestamp(d)

        def get_image(element):
            for child in [e for e in element if e.tag == "Content"]:
                return child.text

        def get_text(element):
            for child in [e for e in element if e.tag == "TEXTE"]:
                return child.text

        tags = [
            "BULLETINS_NEIGE_AVALANCHE",
            "CARTOUCHERISQUE",
            "ImageEnneigement",
            "ImageCartoucheRisque",
            "QUALITE",
        ]
        for element in [e for e in tree.iter() if e.tag in tags]:
            if element.tag == "BULLETINS_NEIGE_AVALANCHE":
                self.name = element.get('MASSIF').title()
                self.date = get_timestamp(element.get('DATEBULLETIN'))
                self.validity = get_timestamp(element.get('DATEVALIDITE'))

            elif element.tag == "ImageCartoucheRisque":
                self.risk_img = get_image(element)
            elif element.tag == "ImageEnneigement":
                self.snow_img = get_image(element)
            elif element.tag == "QUALITE":
                self.snow_quality = get_text(element)
            elif element.tag == "CARTOUCHERISQUE":
                for child in [e for e in element]:
                    if child.tag == "NATUREL":
                        self.comment_nat = child.text
                    elif child.tag == "ACCIDENTEL":
                        self.comment_acc = child.text
                    elif child.tag == "RISQUE":
                        self.risk_max = child.get("RISQUEMAXI")

        self.log.info(f'{self} update timestamp')
        self.save()
