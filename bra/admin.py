from django.contrib import admin

from .models import *

from webcuvette.helpers import ts_2_date
import datetime

class BraAdmin(admin.ModelAdmin):

    list_display = ['__str__', 'update', 'name', 'meteofrance_id', 'id']
    ordering = ('meteofrance_id',)
    search_fields = ('name', )

    def update(self, instance):
        return ts_2_date(instance.date)

admin.site.register(Bra, BraAdmin)
