from django.core.management.base import BaseCommand
from webcuvette import settings
from bra.models import Bra

import logging
import time

log = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, **options):
        if settings.DEBUG:
            bra, create = Bra.objects.get_or_create(meteofrance_id=8)
            bra.update_from_api()
            bra.update_from_xml()
            return

        for i in range(23):
            bra, create = Bra.objects.get_or_create(meteofrance_id=i + 1)
            if bra.is_up_to_date():
                continue
            bra.update_from_api()
            bra.update_from_xml()
            time.sleep(30)
            
        # mountains = [m for d in Domain.objects.all() for m in d.mountains.all()]
        # for mountain in list(set(mountains)):
        #     log.info(f'update bra for {mountain}')
        #     mountain.update_bra()
