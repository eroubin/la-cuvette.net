from django.contrib import admin

from .models import *

class DomainAdmin(admin.ModelAdmin):

    list_display = ['__str__']
    autocomplete_fields = ('mountains', )

admin.site.register(Domain, DomainAdmin)

class MountainAdmin(admin.ModelAdmin):

    list_display = ['name', 'skitour_id', 'get_n_bra']
    ordering = ('skitour_id',)
    search_fields = ('name', )
    autocomplete_fields = ('bras', )

    def get_n_bra(self, instance):
        return len(instance.bras.all())

admin.site.register(Mountain, MountainAdmin)
