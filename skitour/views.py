from django.shortcuts import render
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt

import logging
import json
import traceback

from webcuvette import settings
from skitour.models import Domain
from skitour.models import Mountain


def index(request):
    ''' Main index of the website
    1. Load ./index.html
    '''
    context = {
        "domains": Domain.objects.all().order_by('id')
    }
    return render(request, 'index.html', context)

@csrf_exempt
def conditions(request):
    ''' Direct view that returns a list of "conditions"
    1. loop over conditions by mountains in database
    2.1 sort them in a nested dictionnary data -> massif -> conditions
    2.2 extract unique authors id -> pseudo
    2.3 compile mountain/versant ratings
    3. load ./skitour/conditions.html template

    Note: This view is meant to be called on the client side with javascript.
    '''
    log = logging.getLogger(__name__)
    http_status = 200

    try:

        # get domain id from payload
        payload = json.loads(request.body)
        log.debug(f'request body: {payload}')
        domain_id = int(payload["domain_id"])

        # cache
        cache_key = f'conditions-{domain_id}'
        context = False if settings.DEBUG else cache.get(cache_key)
        if context:
            log.info(f'[cache] hit {cache_key}')

        else:
            domain = Domain.objects.filter(pk=domain_id).first()
            if domain is None:
                raise Exception(f"Unknown domain id {domain_id}")

            log.debug(f'domain: {domain}')

            # loop over mountains in the database
            all_conditions = {}
            all_authors = {}
            mountains = {}
            for mountain in domain.mountains.all():
                log.debug(f'Mountain {mountain}')
                m = mountain.name
                k = mountain.skitour_id

                mountains[k] = {
                    "name": m,
                    "risk_icon": mountain.get_risk_max_icon(),
                    "ratings": {}
                }

                for c in json.loads(mountain.conditions):

                    # nested dictionnary keys
                    d = c["date"]

                    # create nested dictionnary
                    if d not in all_conditions:
                        all_conditions[d] = {}
                    if m not in all_conditions[d]:
                        all_conditions[d][m] = []

                    # fill conditions/authors dictionnaries
                    all_conditions[d][m].append(c)
                    try:
                        all_authors[c["author"]["id"]] = c["author"]["pseudo"]
                    except Exception:
                        # sometimes author is empty :(
                        pass

                    # compute ratings
                    for i, orientation in enumerate(c["orientation"]):

                        # check skiability
                        if c["skiability"].isdigit():
                            skiability = float(c["skiability"])
                        else:
                            log.warning(f'skiability is not a digit for condition {c["id"]} {orientation}: {c["skiability"]}')
                            continue

                        if orientation not in mountains[k]["ratings"]:
                            mountains[k]["ratings"][orientation] = {
                                "total_rating": 0.0,
                                "number_ratings": 0
                            }

                        mountains[k]["ratings"][orientation]["total_rating"] += float(skiability)
                        mountains[k]["ratings"][orientation]["number_ratings"] += 1

            # sort conditions by date
            all_conditions = sorted(
                all_conditions.items(),
                key=lambda d: d[0],
                reverse=True
            )

            # create context
            if not len(all_conditions):  # handle empty list
                context = {"conditions": {
                    "error": "empty list",
                    "msg": "No recent data available",
                }}
            else:  # all good
                # compute global ratings
                context = {
                    "conditions": all_conditions,
                    "authors": all_authors,
                    "mountains": mountains,
                    "domain": domain,
                    "orientations": ["N", "E", "S", "W"]
                }

            cache.set(cache_key, context, settings.LOW_LEVEL_CACHE)
            log.info(f'[cache] set {cache_key}')

    except Exception as e:
        http_status = 500
        context = {"error": {
            "error": e,
            "msg": "Server error",
        }}
        log.error(traceback.format_exc())

    return render(request, 'skitour/conditions.html', context, status=http_status)
