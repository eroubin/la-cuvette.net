from django.core.management.base import BaseCommand

from skitour.models import Domain

import logging

log = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-fs', '--from-scratch', action='store_true')

    def handle(self, **options):
        mountains = [m for d in Domain.objects.all() for m in d.mountains.all()]
        for mountain in list(set(mountains)):
            log.info(f'update conditions for {mountain}')
            mountain.update_conditions(from_scratch=options.get("from_scratch", False))
