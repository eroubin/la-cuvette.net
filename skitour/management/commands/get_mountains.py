from django.core.management.base import BaseCommand

from skitour.models import Mountain
from webcuvette.api import skitour_api_call

import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):
    def handle(self, **options):

        mountains = skitour_api_call("massifs")
        for m in mountains:
            defaults = {
                "skitour_id": m["id"],
                "name": m["nom"],
                "groupe": m["groupe"]
            }
            obj, created = Mountain.objects.update_or_create(
                skitour_id=m["id"],
                defaults=defaults
            )
            log.info(f"Mountain {obj} created {created}")