# Generated by Django 4.1.3 on 2022-12-04 21:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bra', '0003_remove_bra_mountain_bra_name'),
        ('skitour', '0009_mountain_meteofrance_id_overwrite_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mountain',
            name='bra',
        ),
        migrations.RemoveField(
            model_name='mountain',
            name='bra_risk',
        ),
        migrations.RemoveField(
            model_name='mountain',
            name='bra_update',
        ),
        migrations.AddField(
            model_name='mountain',
            name='bras',
            field=models.ManyToManyField(to='bra.bra'),
        ),
    ]
