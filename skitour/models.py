import logging
import json
import time

from django.db import models

from webcuvette import settings
from webcuvette.helpers import find_between
from webcuvette.api import skitour_api_call
from bra.models import Bra


class Mountain(models.Model):
    skitour_id = models.IntegerField(default=0)
    name = models.CharField(max_length=64, default="Massif")
    groupe = models.CharField(max_length=64, default="Groupe")

    # from /conditions
    conditions = models.TextField(default="[]")
    conditions_update = models.IntegerField(default=0)

    # bra
    bras = models.ManyToManyField(Bra)

    log = logging.getLogger(__name__ + ".Mountain")

    class Meta:
        order_with_respect_to = 'skitour_id'

    def __str__(self):
        return f"{self.name} [{self.skitour_id}]"

    def get_risk_max(self):
        risks = [bra.risk_max for bra in self.bras.all()]
        return max(risks) if len(risks) else 0

    def get_risk_max_icon(self):
        return f"/media/bra/icons/R{self.get_risk_max()}_70.png"

    def update_conditions(self, from_scratch=False):
        self.log.info(f'Update conditions: {self}')

        # requests on /conditions and /sorties
        payload_conditions = skitour_api_call(f"conditions?m={self.skitour_id}")
        payload_sorties = skitour_api_call(f"sorties?m={self.skitour_id}")

        # check for error
        if 'ERROR' in payload_conditions or 'ERROR' in payload_sorties:
            return False

        # parse payload
        conditions_list = [] if from_scratch else json.loads(self.conditions)
        conditions_ids = [c["id"] for c in conditions_list]
        for cond in payload_conditions:
            if not ("neige" in cond or "skiabilite" in cond):
                self.log.warning(f'Conditions [{cond["id"]}]: skip (no snow/ski data)')
                continue
            elif cond["id"] in conditions_ids:
                self.log.debug(f'Conditions [{cond["id"]}]: skip (already in db)')
                continue

            date = int(cond.get("date", 0))
            self.name = cond["massif"]["nom"]

            self.log.info(f'Conditions [{cond["id"]}]: new entry ({self.name})')

            conditions = {}
            raw_condition = cond.get('conditions', "")

            # get meteo
            a = "Météo/températures :"
            b = "Conditions d'accès/altitude du parking :"
            conditions["meteo"] = find_between(raw_condition, a, b)
            self.log.debug(f'meteo: {conditions["meteo"]}')

            # get access
            a = "Conditions d'accès/altitude du parking :"
            b = "Altitude de chaussage/déchaussage :"
            conditions["access"] = find_between(raw_condition, a, b)
            self.log.debug(f'access: {conditions["access"]}')

            # get chaussage
            a = "Altitude de chaussage/déchaussage :"
            b = "Conditions pour le ski :"
            conditions["chaussage"] = find_between(raw_condition, a, b)
            self.log.debug(f'chaussage: {conditions["chaussage"]}')

            # get ski
            a = "Conditions pour le ski :"
            b = "Activité avalancheuse :"
            conditions["ski"] = find_between(raw_condition, a, b)
            self.log.debug(f'ski: {conditions["ski"]}')

            # avalanche
            a = "Activité avalancheuse :"
            conditions["avalanche"] = find_between(raw_condition, a)
            self.log.debug(f'avalanche: {conditions["avalanche"]}')

            # get author
            author = {}
            depart = {}
            summits = {}
            altitudes = {}

            for sortie in payload_sorties:
                if sortie["id"] == cond["id"]:
                    sid = cond["id"]
                    self.log.debug(f'Sortie {sid} found')

                    # get author and denivelation from /sorties
                    author = sortie.get("auteur", {})

                    # get depart/altitude from /sortie
                    payload_sortie = skitour_api_call(f"sortie/{sid}")

                    # check for error
                    if 'ERROR' in payload_sortie:
                        self.log.warning(f'skip sortie [{sid}] ({payload_sortie["ERROR"]})')
                        continue

                    depart = payload_sortie.get("depart", {})
                    summits = payload_sortie.get("sommets", [])
                    altitudes = {
                        "start": depart.get("altitude", 0),
                        "denivelation": sortie.get("denivele", 0)
                    }

                    break

            if not len(author.get("id", {})):
                self.log.warning(f'Conditions [{cond["id"]}]: not found in sorties (no authors)')

            # save in the list of conditions
            conditions_list.append({
                "id": cond["id"],
                "date": date,
                "title": cond["titre"],
                "orientation": cond["orientation"],
                "skiability": cond["skiabilite"],
                "snow": cond.get('neige', {}),
                "conditions": conditions,
                "author": author,  # id / pseudo
                "depart": depart,  #
                "summits": summits,  # list of id / nom / altitude
                "altitudes": altitudes,  # start / denivelation
            })

        # cleaning old conditions
        n_days = settings.HISTORY_DAYS
        time_threshold = int(time.time())  # current time
        time_threshold -= int(time.time()) % 86400  # rounded to the day
        time_threshold -= 2 * 3600  # minus 2 hours to handle UTC vs Paris time
        time_threshold -= 86400 * n_days  # minus n days
        n_cleaned = len(conditions_list)
        conditions_list = [
            s for s in conditions_list if (time_threshold - s["date"]) < 0
        ]
        n_cleaned -= len(conditions_list)

        # save payload in database
        self.log.info(f'Saving {len(conditions_list)} conditions in {self.name} ({n_cleaned} cleaned)')
        self.conditions = json.dumps(conditions_list)
        self.conditions_update = int(time.time())
        self.save()

        return True


class Domain(models.Model):
    ''' Class to aggregate several Mountains
    ie: Grenoble -> (Vecrors, Chartreuse, Belledone, Taillefer)
    '''
    name = models.CharField(max_length=64, default="Domaine")
    mountains = models.ManyToManyField(Mountain)

    def __str__(self):
        return f'{self.name} [{self.id}]'

    def get_mountains_id(self):
        return [m.pk for m in self.mountains.all()]
