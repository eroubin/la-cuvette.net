from django import template
from django.utils.safestring import mark_safe

from webcuvette.helpers import ts_2_date

register = template.Library()

@register.filter(name='ts_2_date')
def ts_2_date_filter(timestamp, fmt=None):
    return ts_2_date(timestamp, fmt=fmt)

@register.filter(name='skiability')
def skiability(rating):
    if rating.isdigit():
        rating = int(rating)

        # get number of full/half/empty stars
        n_fs = int(rating / 2)
        n_hs = rating - 2 * n_fs
        n_es = 3 - n_hs - n_fs

        # build fontawesome list
        stars = []
        stars += ['<i class="fa-solid fa-star"></i>'] * n_fs
        stars += ['<i class="fa-solid fa-star-half-stroke"></i>'] * n_hs
        stars += ['<i class="fa-regular fa-star"></i>'] * n_es

    else:
        stars = ['<i class="fa-regular fa-star"></i>'] * 3

    return mark_safe(''.join(stars))

@register.filter(name='altitudes')
def altitudes(altitudes):
    try:
        start = f'{altitudes["start"]}m'
        end = f'{int(altitudes["start"]) + int(altitudes["denivelation"])}m'
        html = f'{start}<i class="fa-solid fa-arrow-up-right-dots mx-1"></i>{end}'
        return mark_safe(html)
    except Exception as e:
        return None

@register.filter(name='summits')
def summits(summits):
    if not len(summits):
        return None
    html = '<i class="fa-solid fa-mountain me-2"></i>'
    html += ', '.join(f'<span title="Altitude: {s["altitude"]}m">{s["nom"]}</span>' for s in summits)
    return mark_safe(html)

@register.filter(name='avalanche')
def avalanche(avalanche):
    search_ras = avalanche.lower()
    for rep in [".", ",", ":", "!"]:
        search_ras = search_ras.replace(rep, "")
    ras = "ras" in [a for a in search_ras.split()]
    cl = 'avalanche-ras' if ras else 'avalanche-not-ras'
    html = f'<i class="fa-solid fa-hill-avalanche {cl}"></i> {avalanche}'
    return mark_safe(html)

@register.filter(name='authors')
def authors(authors):
    def author_link(i, p):
        return f'<a href="https://skitour.fr/membres/{i}" target="_blank">{p}</a>'
    html = ", ".join([author_link(i, p) for i, p in authors.items()])
    return mark_safe(html)

@register.filter(name='mountain_ratings')
def mountain_ratings(ratings, orientation=None):
    try:
        p = ratings[orientation]["total_rating"] / ratings[orientation]["number_ratings"]
        return mark_safe(f'{p:.1f}')
    except Exception as e:
        return ''
