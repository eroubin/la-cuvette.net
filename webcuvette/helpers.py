def clean(string):
    for a, b in [("&nbsp;", " "), ("\\n", " ")]:
        string = string.replace(a, b)
    return string.strip()

def find_between( s, first, last=None ):
    ''' Helper function to parse API payload

    Returns the substring between two strings
    '''
    try:
        if last:
            start = s.index( first ) + len( first )
            end = s.index( last, start )
            return clean(s[start:end])
        else:
            start = s.index( first ) + len( first )
            return clean(s[start:])

    except ValueError:
        return ""

def ts_2_date(timestamp, fmt=None):
    from django.utils.translation import activate
    from django.template.defaultfilters import date
    import datetime
    import pytz

    if not timestamp:
        return "N/A"

    d = datetime.datetime.fromtimestamp(
        timestamp,
        tz=pytz.timezone("Europe/Paris")
    )

    if fmt is None:
        activate('fr')
        return  date(d, 'l d F Y').title()
    elif fmt == 'bra':
        activate('fr')
        return  date(d, 'l d F Y \à H\h')
    else:
        return d.strftime(fmt)
