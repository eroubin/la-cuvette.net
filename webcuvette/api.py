from webcuvette import settings
import requests
import json
import logging
from slugify import slugify
import re

if __name__ == "__main__":
    from settings import SKITOUR_KEY
else:
    from webcuvette.settings import SKITOUR_KEY

log = logging.getLogger(__name__)


class MyDecoder(json.JSONDecoder):
    def decode(self, s, **kwargs):
        # hack 1 to handle to "I don't understand how it's different from \n"
        s = '\\n'.join(s.splitlines())

        # hack 2 for some reason some ' are escaped with \'
        s = s.replace("\\'", "\'")

        # hack 3 remove tabs and multiple spaces
        # s = ' '.join(s.split())
        s = re.sub(' +', ' ', s)

        return super().decode(s, **kwargs)


def skitour_api_call(path, key=SKITOUR_KEY, debug=True):

    # request on /conditions
    url = f"https://skitour.fr/api/{path}"
    headers = {"cle": key}
    log.debug(f'call {url} / {headers}')

    # make the initial call
    response = requests.get(url, headers=headers)

    # test status
    try:
        response.raise_for_status()
    except requests.HTTPError as e:
        log.error(f'HTTP Error {url}: {e} (ignoring call)')
        return {"ERROR": str(e)}

    # desirialize
    try:
        # if debug:
        #     with open(f'logs/payload-{slugify(path)}-unsanitized-b.txt', 'w+') as f:
        #         f.write(str(response.content))
        #     with open(f'logs/payload-{slugify(path)}-unsanitized-s.txt', 'w+') as f:
        #         f.write(response.text)

        j = response.json()

    except requests.JSONDecodeError:

        try:
            j = response.json(cls=MyDecoder)

        except requests.JSONDecodeError as e:
            if debug:
                with open(f'logs/payload-{slugify(path)}-unsanitized-b.txt', 'w+') as f:
                    f.write(str(response.content))
                with open(f'logs/payload-{slugify(path)}-unsanitized-s.txt', 'w+') as f:
                    f.write(response.text)

            log.error(f'API error {url}: {str(e)} (ignoring call)')
            return {"ERROR": str(e)}

    # check if for API error
    if j is None:
        log.error(f'API error {url}: empty payload (ignoring call)')
        return {"ERROR": "empty payload"}

    if 'ERROR' in j:
        log.error(f'API error {url}: {j["ERROR"]} (ignoring call)')

    return j


def meteofrance_api_call(method, urls):
    import urllib3
    urllib3.disable_warnings()

    # Example of a Python implementation for a continuous authentication client.
    # It's necessary to :
    # - update APPLICATION_ID
    # - update request_url at the end of the script

    # unique application id : you can find this in the curl's command to generate jwt token

    APPLICATION_ID = settings.METEOFRANCE_APPLICATION_ID

    class Client(object):
        def __init__(self):
            self.session = requests.Session()

        def request(self, method, url, **kwargs):

            # First request will always need to obtain a token first
            if 'Authorization' not in self.session.headers:
                self.obtain_token()

            # Optimistically attempt to dispatch reqest
            response = self.session.request(method, url, **kwargs)
            if self.token_has_expired(response):
                # We got an 'Access token expired' response => refresh token
                self.obtain_token()
                # Re-dispatch the request that previously failed
                response = self.session.request(method, url, **kwargs)

            return response

        def token_has_expired(self, response):
            status = response.status_code
            content_type = response.headers['Content-Type']
            repJson = response.text
            if status == 401 and 'application/json' in content_type:
                repJson = response.text
                if 'Invalid JWT token' in repJson['description']:
                    return True
            return False

        def obtain_token(self):
            # Obtain new token
            data = {'grant_type': 'client_credentials'}
            headers = {'Authorization': 'Basic ' + APPLICATION_ID}
            access_token_response = requests.post("https://portail-api.meteofrance.fr/oauth2/token", data=data, verify=False, allow_redirects=False, headers=headers)
            token = access_token_response.json()['access_token']
            # Update session with fresh token
            self.session.headers.update({'Authorization': 'Bearer %s' % token})

    # main
    client = Client()
    client.session.headers.update({'Accept': 'application/json'})
    return [client.request(method, url, verify=False) for url in urls]


if __name__ == "__main__":
    response = skitour_api_call("conditions?m=9", debug=True)
    if "ERROR" in response:
        log.info(response)
    else:
        log.info("success")

    # response = skitour_api_call("sortie/159975")
    # if "ERROR" in response:
    #     log.info(response)
    # else:
    #     log.info("success")
