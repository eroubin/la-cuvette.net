// doc ready
function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1);
  } else {
      document.addEventListener("DOMContentLoaded", fn);
  }
}

// get to top
let toTopButton = document.getElementById("btn-back-to-top");

// When the user scrolls down 1080px from the top of the document, show the button
let changeProfilePict = true;
window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (
        document.body.scrollTop > 1080 ||
        document.documentElement.scrollTop > 1080
    ) {
        toTopButton.style.display = "block";
    } else {
        toTopButton.style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
toTopButton.addEventListener("click", backToTop);

function backToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
