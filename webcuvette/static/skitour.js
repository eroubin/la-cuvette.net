docReady(function () {
    console.log("DOm loaded");
    // get anchor
    const hash = window.top.location.hash.substr(1);
    // inject anchor id or 1
    injectConditions(hash || 1);
    // add frame to active page
    document.querySelectorAll('.nav-link').forEach(targetNode => {
        if(targetNode.dataset.did == (hash || 1)) {
            targetNode.classList.add('nav-active');
        }
    });
});

// pills
document.querySelectorAll('.nav-link').forEach(targetNode => {
    // on click: load domain
    targetNode.addEventListener("click", function (event) {
        event.preventDefault()
        injectConditions(event.target.dataset.did);
        // a -> il -> ul -> first il
        let active = event.target.parentNode.parentNode.firstElementChild
        while(active) {  // loop over il
            // remove il -> a.class active
            active.firstChild.classList.remove('nav-active');
            // move to next il
            active = active.nextElementSibling;
        }
        event.target.classList.add("nav-active");
    }, false);

});

function injectConditions(domain_id) {
    console.log("Inject domain " + domain_id)
    const inj = document.querySelector('#inject-skitour')
    inj.innerHTML = '<i class="fas fa-spinner fa-spin"></i>'

    if(domain_id == 0){
        fetch("/get/bras/", {method: 'get'}).then(r => {
            return r.text()
        }).then(html => {
            inj.innerHTML = html
        })
        return
    }

    fetch("/post/conditions/", {
        method: 'post',
        body: JSON.stringify({"domain_id": domain_id})
    }).then(function (response) {
        return response.text();
    }).then(function (data) {
        inj.innerHTML = data;
    }).catch(function () {
        console.log("catch skitour injection")
    }).then(function () {

        // Handle clamp

        document.querySelectorAll('.ski-condition-clamp').forEach(targetNode => {
            if (targetNode.offsetHeight < targetNode.scrollHeight ||
                targetNode.offsetWidth < targetNode.scrollWidth) {

                // change css
                targetNode.style.cursor = 'pointer'
                targetNode.classList.add("ski-clamped")

                // event listener on the button
                targetNode.addEventListener("click", function (event) {

                    // expand or clamp text
                    if (targetNode.classList.contains("ski-condition-clamp")) {
                        targetNode.classList.remove("ski-condition-clamp")
                        targetNode.classList.remove("ski-clamped")
                    } else {
                        targetNode.classList.add("ski-condition-clamp")
                        targetNode.classList.add("ski-clamped")
                    }

                    // flashy flash flash
                    parentDiv = targetNode.parentNode
                    targetNode.parentNode.classList.add("list-hover-flash");
                    setTimeout(function () {
                        parentDiv.classList.remove("list-hover-flash");
                    }, 1000);
                })

            } else {
                // your targetNode doesn't overflow (not truncated)
            }
        })

        // Handle BRA modals

        document.querySelectorAll('.bra-modal-trigger').forEach(item => {
            item.addEventListener('click', event => {
                let modal_id = item.dataset.key
                console.log("bra trigger " + modal_id)
                let m = document.getElementById('bra-modal-' + modal_id)
                bra = new bootstrap.Modal(m);
                bra.show()

                const inj = document.querySelector('#inject-bra-' + modal_id)
                inj.innerHTML = '<i class="fas fa-spinner fa-spin"></i>'
                fetch("/post/bra/", {
                    method: 'post',
                    body: JSON.stringify({"mountain_id": inj.dataset.mid})
                }).then(function (response) {
                    return response.text();
                }).then(function (data) {
                    inj.innerHTML = data;
                }).catch(function () {
                    console.log("catch bra injection")
                });
            })
        })

    });
};